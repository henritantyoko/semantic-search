<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous" />
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css" />

  <!-- Bootstrap Icons -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css" />

  <!-- My CSS -->
  <link rel="stylesheet" href="src/style.css" />

  <title>Semantic Web</title>
</head>

<body id="home">
  <!-- Navbar -->
  <!--PHP-->
  <?php
  require_once("sparqllib.php");
  $test = "";
  if (isset($_POST['search-university'])) {
    $test = $_POST['search-university'];
    $data = sparql_get(
      "http://localhost:3030/university",
      "
    PREFIX d:<http://www.semanticweb.org/henri/ontologies/2023/5/university#>

    SELECT ?Nidn ?NamaDosen ?NamaMahasiswa ?Matkul
    WHERE {
    ?dsn 	d:nidn ?Nidn;
            d:namaDosen ?NamaDosen;
            d:mengajar ?mhs.
    ?mhs	d:namaMahasiswa ?NamaMahasiswa;
          d:mengambil ?mtkul.
    ?mtkul	d:namaMatkul ?Matkul.
    FILTER (regex (?NamaDosen,  '$test', 'i') || regex (?Nidn,  '$test', 'i') || regex (?NamaMahasiswa,  '$test', 'i') || regex (?Matkul,  '$test', 'i'))
    }"
    );
  } else {
    $data = sparql_get(
      "http://localhost:3030/university",
      "
    PREFIX d:<http://www.semanticweb.org/henri/ontologies/2023/5/university#>

    SELECT ?Nidn ?NamaDosen ?NamaMahasiswa ?Matkul
    WHERE {
    ?dsn 	d:nidn ?Nidn;
            d:namaDosen ?NamaDosen;
            d:mengajar ?mhs.
    ?mhs	d:namaMahasiswa ?NamaMahasiswa;
          d:mengambil ?mtkul.
    ?mtkul	d:namaMatkul ?Matkul.
    }"
    );
  }

  if (!isset($data)) {
    print "<p>Error: " . sparql_errno() . ": " . sparql_error() . "</p>";
  }

  ?>
  <!-- Akhir Navbar -->

  <!-- About -->
  <section id="about">
    <div class="container">


      <div class="row tentang">
        <div class="text-center mt-5 ">
          <form action="" method="post" id="nameform">
            <div class="search-box">
              <input type="text" name="search-university" placeholder="Cari Tim" />
              <button type="submit" class="btn btn-primary">Cari</button>
            </div>
            <i class="bi bi-search"></i>
        </div>
        </form>
      </div>

      <!-- Hasil Pencarian -->

      <div class="row text-center mb-3 mt-0 hasil">
        <div class="col">
          <h2>Cari Tim Kamu Disini!</h2>
        </div>
      </div>
      <div class="row fs-5">
        <div class="col-md-5">
          <p>
            Menampilkan Pencarian :
            <br />
          </p>
          <p>
            <span>
              <?php
              if ($test != NULL) {
                echo $test;
              } else {
                echo "Hasil Pencarian Kamu :";
              }
              ?></span>
          </p>
        </div>
      </div>

      <div class="row">

        <?php $i = 0; ?>
        <?php foreach ($data as $dat) : ?>
          <div class="col-md-4">
            <div class="box">
              <ul class="list-group list-group-flush">
                
                <div class="header-data"> <b>Nidn :</b></div>
                <div class="item-data"><?= $dat['Nidn'] ?></div>

                <div class="header-data"> <b>Nama Dosen :</b></div>
                <div class="item-data"><?= $dat['NamaDosen'] ?></div>

                <div class="header-data"> <b>Nama Mahasiswa :</b></div>
                <div class="item-data"><?= $dat['NamaMahasiswa'] ?></div>

                <div class="header-data"> <b>Matkul :</b></div>
                <div class="item-data"><?= $dat['Matkul'] ?></div>

              </ul>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
  <!-- Akhir About -->

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>

</html>